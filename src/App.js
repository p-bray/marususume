import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Body from './components/structuralComponents/Body'
import MyNav from './components/structuralComponents/MyNav'

class App extends Component {
  render() {
    return (
      <div className='backgroud-div'>
        <MyNav/>
        <div className='container'>
          <Body/>
        </div>
      </div>
    );
  }
}

export default App;
